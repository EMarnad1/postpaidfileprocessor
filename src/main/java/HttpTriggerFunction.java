import avro.LineItem;
import avro.Row;

import com.microsoft.azure.functions.*;
import com.microsoft.azure.functions.annotation.AuthorizationLevel;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpTrigger;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;
import com.microsoft.azure.servicebus.*;
import com.microsoft.azure.servicebus.primitives.ConnectionStringBuilder;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageCredentials;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;

import org.apache.avro.data.Json;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.Logger;
import org.apache.avro.io.JsonEncoder;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.time.Duration;
import java.util.concurrent.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.logging.log4j.LogManager.getLogger;

public class HttpTriggerFunction {
    public static String[] fields;
    public static final String delimiter = "\\|";

    public static AtomicInteger waitGroup;
    public static ConcurrentLinkedQueue<String[]> fileLines;
    public static ConcurrentLinkedQueue<LineItem> lineItems;
    public static AtomicBoolean parsingDone;

    static Logger log = getLogger("Logger");

    public static class sendToServiceBus extends RecursiveAction {
        int waitLength, numMessagesPerWait, messagesSent;
        String connectionString, queueName;

        public sendToServiceBus(int waitLength, int numMessagesPerWait, String connectionString, String queueName) {
            this.waitLength = waitLength;
            this.numMessagesPerWait = numMessagesPerWait;
            this.messagesSent = 0;
            this.connectionString = connectionString;
            this.queueName = queueName;
        }

        public CompletableFuture<Void> sendMessagesAsync(QueueClient sendClient) throws IOException {
            //create ByteOutputStream, JsonEncoder and Writer to write Avro Object as bytes
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            JsonEncoder jsonEncoder = EncoderFactory.get().jsonEncoder(LineItem.getClassSchema(), stream);
            DatumWriter<LineItem> writer = new SpecificDatumWriter<>(LineItem.class);
            //tasks to send to service bus asynchonously
            List<CompletableFuture> tasks = new ArrayList<>();
            //keep in loop until whole file is parsed or until both queues are empty
            while (!parsingDone.get() || !lineItems.isEmpty() || !fileLines.isEmpty()) {
                LineItem lineItem = lineItems.poll();
                try {
                    if (lineItem != null) {
                        //buffer period after certain number of messages sent
                        //adjust based on performance of service bus
                        if (messagesSent % numMessagesPerWait == 0)
                            TimeUnit.SECONDS.sleep(waitLength);
                        //convert to bytes
                        writer.write(lineItem, jsonEncoder);
                        //set message attributes
                        Message message = new Message(stream.toByteArray());
                        message.setContentType("application/avro");
                        message.setLabel("Record");
                        message.setMessageId(String.valueOf(messagesSent++));
                        message.setTimeToLive(Duration.ofMinutes(2));
                        //flush writers
                        jsonEncoder.flush();
                        stream.flush();
                        try {
                            CompletableFuture<Void> task = sendClient.sendAsync(message);
                            tasks.add(task);
                        } catch (Exception e) {
                            log.error(e.getMessage());
                        }
                    }

                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            }
            return CompletableFuture.allOf(tasks.toArray(new CompletableFuture<?>[tasks.size()]));
        }

        public void compute() {
            waitGroup.incrementAndGet();
            log.info("Sending to service bus");
            StopWatch watch = new StopWatch();
            watch.start();
            QueueClient sendClient;
            try {
                //send messages to service bus
                sendClient = new QueueClient(new ConnectionStringBuilder(connectionString, queueName), ReceiveMode.PEEKLOCK);
                sendMessagesAsync(sendClient).thenRunAsync(() -> sendClient.closeAsync());
                sendClient.close();
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            } catch (ServiceBusException e) {
                log.error(e.getMessage());
            } catch (IOException e) {
                log.error(e.getMessage());
            }
            log.info(String.format("Done with service bus, took %d seconds", watch.getTime() / 1000));
            waitGroup.decrementAndGet();
        }
    }

    public static class turnToJSonTask extends RecursiveAction {
        int recordsPerMessage;

        public turnToJSonTask(int recordsPerMessage) {
            this.recordsPerMessage = recordsPerMessage;
        }

        public void compute() {
            waitGroup.incrementAndGet();
            StopWatch watch = new StopWatch();
            watch.start();
            log.info("Starting Avro conversion");
            //create array of Avro objects
            LineItem lineItem = new LineItem();
            List<Row> rows = new ArrayList<>();
            //wait until whole file is looked at or until queue of records is empty
            StopWatch sw = new StopWatch();
            sw.start();
            int count = 0;
            while (!parsingDone.get() || !fileLines.isEmpty()) {
                String[] line = fileLines.poll();
                if (line != null) {
                    Row row = new Row();
                    count++;
                    //creating the Avro object
                    for (int field = 0; field < line.length; field++) {
                        row.put(fields[field], line[field]);
                    }
                    //placing into the array
                    rows.add(row);
                    //if array is full, create a new thread to
                    //send to service bus
                    if (rows.size() == recordsPerMessage || sw.getTime() > 1000) { //change to i > 0 and result[i][2] != result[i - 1][2]
                        try {
                            lineItem.put("lineItem", rows);
                        } catch (Exception e) {
                            log.error(e.getMessage());
                        }
                        //add AvroObject to queue
                        lineItems.add(lineItem);
                        //create new array
                        lineItem = new LineItem();
                        rows = new ArrayList<>();
                        sw.reset();
                        sw.start();
                    }
                }
            }
            log.info(String.format("Done with Avro conversion, took %d seconds", watch.getTime() / 1000));
            log.info("Count: " + count);
            waitGroup.decrementAndGet();
        }
    }

    public static void getFileFromBlob(String filename, String connectionString, String containerName) {
        try {
            //instantiating blob client
            StorageCredentials credentials = StorageCredentials.tryParseCredentials(connectionString);
            CloudStorageAccount account = new CloudStorageAccount(credentials, true);
            CloudBlobClient client = account.createCloudBlobClient();
            //opening file to write to
            File file = new File(filename);
            CloudBlobContainer container = client.getContainerReference(containerName);
            CloudBlob blob = container.getBlockBlobReference(filename);
            FileOutputStream f = new FileOutputStream(file);
            //downloading container contents to file
            blob.download(f);
            f.close();
            //delete blob as we do not need it anymore
            //blob.delete();
            readFile(filename);
        } catch (URISyntaxException | IOException | InterruptedException | InvalidKeyException | StorageException e) {
            log.error(e.getMessage());
        }
    }

    public static void readFile(String filename) throws IOException, InterruptedException {
        StopWatch watch = new StopWatch();
        watch.start();
        log.info(String.format("Starting %s file read", filename));

        BufferedReader reader;
        reader = new BufferedReader(new FileReader(filename));
        String line = reader.readLine();

        //setting fields for the file
        fields = line.split(delimiter);
        for (int i = 0; i < fields.length; i++) {
            fields[i] = fields[i].trim();
        }
        //split lines into String array based on delimiter
        line = reader.readLine();
        while (line != null) {
            fileLines.add(line.split(delimiter));
            line = reader.readLine();
        }
        TimeUnit.SECONDS.sleep(10);
        reader.close();
        log.info(String.format("Done with %s file read, took %d seconds", filename, watch.getTime() / 1000));
        parsingDone.set(true);
    }

    public static class StartTasks extends Thread {
        String filename;

        public StartTasks(String filename) {
            this.filename = filename;
        }

        public void run() {
            //initializing global variables
            log.info("Initializing all variables and tasks");
            fileLines = new ConcurrentLinkedQueue<>();
            lineItems = new ConcurrentLinkedQueue<>();
            waitGroup = new AtomicInteger(0);
            parsingDone = new AtomicBoolean(false);
            File configFile = new File("config.properties");
            FileReader reader = null;
            try {
                reader = new FileReader(configFile);
                Properties props = new Properties();
                props.load(reader);

                //setting variables from config file
                int recordsPerMessage = Integer.parseInt(props.getProperty("recordsPerMessage"));
                int waitTime = Integer.parseInt(props.getProperty("waitLength"));
                int numMessagesPerWait = Integer.parseInt(props.getProperty("numMessagesPerWait"));
                String blobConnectionString = props.getProperty("blobConnectionString");
                String busConnectionString = props.getProperty("busConnectionString");
                String queueName = props.getProperty("queueName");
                String containerName = props.getProperty("containerName");

                //start tasks concurrently
                turnToJSonTask jsonTask = new turnToJSonTask(recordsPerMessage);
                sendToServiceBus serviceBusTask = new sendToServiceBus(waitTime, numMessagesPerWait, busConnectionString, queueName);
                ForkJoinPool pool = new ForkJoinPool();
                pool.execute(jsonTask);
                pool.execute(serviceBusTask);
                getFileFromBlob(filename, blobConnectionString, containerName);
            } catch (Exception e) {
                log.error(e.getMessage());
            } finally {
                if(reader != null) {
                    try {
                        reader.close();
                    } catch(IOException e){
                        log.error(e.getMessage());
                    }
                }
            }
            //wait until all tasks are done to exit main thread
            while (waitGroup.get() > 0) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    }
            }
        }
    }

    @FunctionName("HttpTrigger-Java")
    public HttpResponseMessage run(
            @HttpTrigger(name = "req", methods = {HttpMethod.GET, HttpMethod.POST}, authLevel = AuthorizationLevel.ANONYMOUS) HttpRequestMessage<Optional<String>> request,
            final ExecutionContext context) throws IOException, InterruptedException {
        context.getLogger().info("Java HTTP trigger processed a request.");

        // Parse query parameter
        String query = request.getQueryParameters().get("filename");
        String filename = request.getBody().orElse(query);

        //start tasks if filename given
        if (filename == null) {
            return request.createResponseBuilder(HttpStatus.BAD_REQUEST).body("Please pass a filename on the query string or in the request body").build();
        } else {
            //start tasks
            StartTasks startTasks = new StartTasks(filename);
            startTasks.start();
            return request.createResponseBuilder(HttpStatus.OK).body("Success").build();
        }
    }
}